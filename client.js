/**
 * Find the most active property in data
 * @param {Array<Object>} data 
 * @param {string} property 
 * 
 */
const mode = (data,property) =>{
    let map = {}
    let maxCount = 1;

    // remove the null values for the specific property
    data = data.filter(d => d[property] != null)

    // remove all other properties 
    let nums = data.map(d => d[property])

    // keep track of most frequently occurring property value
    let mostActive = nums[0]
    nums.forEach(n => {
        if(map[n] == null){
            map[n]=1
        }else{
            map[n]++
        }
        if(map[n] > maxCount)
        {
            mostActive = n;
            maxCount = map[n];
        }
    })
    return mostActive
}

/**
 * Find all the unique battle types
 * @param {Array<Object>} data 
 */
const unique = (data) =>{

    // remove the empty string values
    data = data.filter(d => d.battle_type != "")

    // return a set of all unique/distinct values
    return [... new Set(data.map(d => d.battle_type))]
}

/**
 * Find the max value of the property
 * @param {Array<Object>} data 
 * @param {String} property 
 */
const max = (data,property) => {

    // remove the null values for the specific property    
    data = data.filter(d => d[property] != null)

    // remove all other properties 
    let nums = data.map(d => d[property])
    return Math.max(...nums)
}

/**
 * Find the min property of property
 * @param {Array<Object>} data 
 * @param {String} property 
 */
const min = (data,property) => {
    
    // remove the null values for the specific property
    data = data.filter(d => d[property] != null)
    //console.log('data',data)

    // remove all other properties 
    let nums = data.map(d => d[property])

    // return min value
    return Math.min(...nums)
}

/**
 * Find the average value for the property
 * @param {Array<Object>} data 
 * @param {String} property 
 */
const mean = (data,property) => {

    // remove the null values for the specific property
    data = data.filter(d => d[property] != null)

    // calculate the sum of all the values
    let sum = data.reduce((sum,d)=> d[property] + sum , 0)

    // return the rounded average value
    return Math.round(sum/data.length)
}

/**
 * Count the number of win and loss
 * @param {Array<Object>} data 
 */
const count = (data) => {
    //console.log('count function')
    let attackerOutcome = {
        win: 0,
        loss: 0
    }

    // count the number of occurrence of win and loss
    data.forEach(d=>{
        if(d.attacker_outcome == "win") attackerOutcome['win']++;
        else attackerOutcome['loss']++;
    })
    //console.log('attacker outcome',attackerOutcome)
    return attackerOutcome
}

/**
 *  Handle click event
 * Fetch and display the json data
 */
$('#get-json').on('click',()=>{
    let json = {}
    $.ajax({
        url: 'battles.json',
        method: 'GET',
        success: function(data){
            //console.log(data)

            // find the most active value 
            json['most_active'] = {}
            json['most_active']['attacker_king'] = mode(data,"attacker_king")
            json['most_active']['defender_king'] = mode(data,"defender_king")
            json['most_active']['region'] = mode(data,"region")
            json['most_active']['name'] = mode(data,"name")

            // count the number of win and loss
            json['attacker_outcome'] = count(data)

            // find the distinct battle types
            json['battle_type'] = unique(data)

            // find the average, min and max for "defender_size" property
            json['defender_size'] = {}
            json['defender_size']['average'] = mean(data,"defender_size")
            json['defender_size']['max'] = max(data,"defender_size")
            json['defender_size']['min'] = min(data,"defender_size")

 
            $('#json').text(JSON.stringify(json,null,4))
            // console.log('Max defender size',max(data,"defender_size"))
            // console.log('Min defender size',min(data,"defender_size")) 
            // console.log('Most Active ',"attacker_king", mode(data,"attacker_king"))
            // console.log('Most Active ',"defender_king", mode(data,"defender_king"))
            // console.log('Most Active ',"region", mode(data,"region"))
            // console.log('Most Active ',"name", mode(data,"name"))
            // console.log('Unique battle types', unique(data))
        },
        error: function(err){
            alert('Something went wrong')
        }
    })
})